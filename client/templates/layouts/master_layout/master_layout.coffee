Template.masterLayout.onRendered ->
  @autorun ->
    FlowRouter.watchPathChange()
    $('body').attr 'page', FlowRouter.current().route.name
