Template.registerHelper 'routePath', (routeName, params, queryParams) ->
  FlowRouter.path routeName, params, queryParams

Template.registerHelper 'isCurrentRoute', (routeName, returnIfTrue = true, returnIfFalse = false) ->
  FlowRouter.watchPathChange()
  if FlowRouter.current().route.name == routeName then returnIfTrue else returnIfFalse

Template.registerHelper 'getUser', (userId) ->
  Meteor.users.findOne userId

Template.registerHelper 'isThisMe', (userId) ->
	Meteor.userId() == userId
