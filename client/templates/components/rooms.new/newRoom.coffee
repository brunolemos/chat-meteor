Template.newRoom.events
  'keyup form[name=newRoom] input': (e, tmpl) ->
    Session.set 'newRoom',
      name: $('form[name=newRoom] input[name=room]').val() || 'New room'
      description: $('form[name=newRoom] input[name=description]').val()

  'submit form[name=newRoom]': (e, tmpl) ->
    e.preventDefault()

    room =
      name: e.target.room.value
      description: e.target.description.value
      createdBy: Meteor.userId()
      createdAt: new Date()

    Rooms.insert room, (error, _id) ->
      alert error if error
      FlowRouter.go 'room', _id: _id if _id

    e.target.reset()


Template.newRoom.helpers
  'newRoom': ->
    Session.get 'newRoom'

Template.newRoom.onCreated ->
  Session.set 'newRoom', {name: 'New room', description: ''}

Template.newRoom.onDestroyed ->
  Session.set 'newRoom', null
