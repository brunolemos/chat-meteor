Template.rooms.helpers
  'rooms': ->
    Rooms.find {}, sort: createdAt: -1

  'roomClass': (room) ->
    if FlowRouter.getParam('_id') == room._id then 'active' else ''
