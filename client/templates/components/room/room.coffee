Template.room.helpers
  'room': ->
    Rooms.findOne Session.get 'roomId'

  'messageGroups': ->
    messages = Messages.find({roomId: Session.get 'roomId'}, sort: createdAt: 1).fetch()

    lastUserId = null
    groupedMessages = []
    result = []
    for message in messages
      if lastUserId && message.userId != lastUserId  && groupedMessages.length > 0
        result.push groupedMessages
        groupedMessages = []

      groupedMessages.push message
      lastUserId = message.userId

    if groupedMessages.length > 0
      result.push groupedMessages

    return result

  'messageClass': (message) ->
    if Meteor.userId() == message.userId then 'sent' else 'received'

Template.room.events
  'submit form#newMessageForm': (e, tmpl) ->
    e.preventDefault()

    Messages.insert
      roomId: Session.get 'roomId'
      userId: Meteor.userId()
      text: e.target.message.value
      createdAt: new Date()

    e.target.reset()

  'click #deleteRoom': (e, tmpl) ->
    e.preventDefault()

    # Messages.remove roomId: $(e.target).data('id')
    Rooms.remove $(e.target).data('id'), (error) ->
      if !error
        redirectUrl = '/'

        if Rooms.find().count() > 0
          firstRoom = Rooms.find({}, {sort: {createdAt: -1}, limit: 1}).fetch()[0]
          redirectUrl = FlowRouter.path 'room', _id: firstRoom._id

        FlowRouter.redirect redirectUrl

Template.room.onCreated ->
  @autorun =>
    Session.set 'roomId', FlowRouter.getParam('_id')
