Template.login.events
  'submit form[name=login]': (e, tmpl) ->
    e.preventDefault()

    user =
      email: e.target.email.value
      password: e.target.password.value

    Meteor.loginWithPassword user.email, user.password, (error) ->
      alert error.reason if error
