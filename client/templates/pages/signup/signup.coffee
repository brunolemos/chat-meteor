Template.signup.events
  'submit form[name=signup]': (e, tmpl) ->
    e.preventDefault()

    if e.target.password.value.length < 3
      alert 'Password must have at least 3 characters'
      return
    else if e.target.password.value != e.target.confirm_password.value
      alert 'Passwords don\'t match'
      return

    user =
      email: e.target.email.value
      password: e.target.password.value
      profile: name: e.target.name.value

    Accounts.createUser user, (error) ->
      alert error.reason if error
