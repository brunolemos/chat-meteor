if Meteor.isClient
  BlazeLayout.setRoot '#root'

redirectIfLoggedIn = (ctx, redirect, stop) ->
  redirect '/' if Meteor.userId()

showLoginIfNotLoggedIn = (ctx, redirect, stop) ->
  if !Meteor.user()
    BlazeLayout.render 'masterLayout', content: 'login' unless Meteor.user()
    stop()

privateRoutes = FlowRouter.group
  name: 'private'
  triggersEnter: [showLoginIfNotLoggedIn]

publicRoutes = FlowRouter.group
  name: 'public'
  triggersEnter: [redirectIfLoggedIn]

Accounts.onLogin ->
  if Meteor.isClient && (FlowRouter.current().path == '/' || FlowRouter.current().route.group.name == 'private')
    FlowRouter.reload()
  else
    FlowRouter.go '/'

FlowRouter.route '/',
  name: 'index'
  action: (params) ->
    if Meteor.user()
      Tracker.nonreactive =>
        if Rooms.find().count() <= 0
          BlazeLayout.render 'masterLayout', content: 'newRoom'
        else
          BlazeLayout.render 'masterLayout', content: 'rooms'
    else
      FlowRouter.go '/login'

publicRoutes.route '/signup',
  name: 'signup'
  action: (params) ->
    BlazeLayout.render 'masterLayout', content: 'signup'

publicRoutes.route '/login',
  name: 'login'
  action: (params) ->
    BlazeLayout.render 'masterLayout', content: 'login'

FlowRouter.route '/logout',
  name: 'logout'
  action: (params) ->
    Meteor.logout ->
      FlowRouter.go '/'
      FlowRouter.reload()

privateRoutes.route '/new',
  name: 'newRoom'
  action: (params) ->
    BlazeLayout.render 'masterLayout',
      content: 'newRoom'

privateRoutes.route '/r/:_id',
  name: 'room'
  action: (params) ->
    BlazeLayout.render 'masterLayout',
      content: 'room'
