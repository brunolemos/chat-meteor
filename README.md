# Chat

## Pre requisites

[Install Meteor](<https://www.meteor.com/install>)

## Run

Open the terminal in the project folder and execute:
```sh
meteor
```

Open <http://localhost:3000> in your browser.
